package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
)

// Config - args strcutre and possibly other configuration
// doit - by default runs in dry run
// move - by default copy not move
// path - which operates on
type Config struct {
	doit    bool
	move    bool
	debug   bool
	srcpath string
	dstpath string
}

var config Config

func main() {
	getArguments()
	files := getFiles(config.srcpath)
	if len(files) == 0 {
		os.Exit(0)
	}
	groupsMap := agregateFiles(files)
	copyToDirectories(groupsMap, "2003-02-01", true)
}

func getArguments() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage: %s [options] destpath\n", filepath.Base(os.Args[0]))
		flag.PrintDefaults()
	}

	doit := flag.Bool("doit", false, "Dont do anything just show me... (default)")
	move := flag.Bool("move", false, "Move files instead of copy")
	debug := flag.Bool("debug", true, "Debug on/off")
	srcpath := flag.String("srcpath", "", "Path with files to group (source), defaults to cwd")

	//flag.String("path", "", "path where groupfiles operates, if not specified last argument is considered as path")
	flag.Parse()

	config.doit = *doit
	config.move = *move
	config.debug = *debug

	log.SetFlags(0)
	if !config.debug {
		log.SetOutput(ioutil.Discard)
	}

	dstpath := flag.Arg(0)
	if dstpath == "" {
		flag.Usage()
		os.Exit(1)
	}
	config.dstpath = getAbsPath(dstpath)

	if *srcpath == "" {
		config.srcpath = getCurrentWorkingDirectory()
	} else {
		config.srcpath = getAbsPath(*srcpath)
	}
}

func getAbsPath(path string) string {
	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Fatal("Path error:", err)
	}
	log.Println("Working with:", absPath)
	return absPath
}

func getCurrentWorkingDirectory() string {
	wd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Working directory", wd)
	return wd
}

func getFiles(directory string) []string {
	concreteFileNamesMOV, _ := filepath.Glob(filepath.Join(directory, "*.MOV"))
	concreteFileNamesRAW, _ := filepath.Glob(filepath.Join(directory, "*.NEF"))
	concreteFileNamesJPEG, _ := filepath.Glob(filepath.Join(directory, "*.JPG"))
	all := append(append(concreteFileNamesJPEG, concreteFileNamesRAW...), concreteFileNamesMOV...)

	log.Printf("Found %d files\n", len(all))

	return all
}

func agregateFiles(files []string) map[string][]string {
	aggregate := make(map[string][]string)
	for _, file := range files {
		fileInfo, err := os.Lstat(file)
		if err != nil {
			log.Printf("Error: File %s can't be readed: %s\n", file, err)
			continue
		}
		y, m, d := fileInfo.ModTime().Date()
		key := fmt.Sprintf("%d-%02d-%02d", y, m, d)
		if _, ok := aggregate[key]; !ok {
			aggregate[key] = []string{file}
		} else {
			aggregate[key] = append(aggregate[key], file)
		}
	}
	log.Printf("Aggregated to %d groups\n", len(aggregate))
	// log.Printf("Debug: %s", aggregate)
	return aggregate
}

func lastFourtyChars(newPath string) string {
	if len(newPath) <= 40 {
		return newPath
	}
	return newPath[len(newPath)-40:]
}

func copyToDirectories(groups map[string][]string, pattern string, dryrun bool) {
nextDate:
	for dateKey, fileNameArray := range groups {
		log.Println("Processing group", dateKey)
		for _, file := range fileNameArray {
			newPath := filepath.Join(config.dstpath, dateKey)
			oldPath := filepath.Dir(file)
			filename := filepath.Base(file)
			if _, err := os.Stat(newPath); err != nil {
				if os.IsNotExist(err) {
					log.Printf("Directory doesn't exist, creating")
					if config.doit {
						err := os.Mkdir(newPath, os.ModePerm)
						if err != nil {
							log.Printf("Some error occured for %s: %s\n", newPath, err)
							continue nextDate
						}
					}
				} else {
					log.Printf("Some error occured for %s: %s\n", newPath, err)
					continue nextDate
				}
			}
			log.Printf("Copying %s to ...%s\n", filename, lastFourtyChars(newPath))
			if config.doit {
				command := []string{"cp", "-pfr"}
				if config.move {
					command = []string{"mv", "-nv"}
				}
				cpCmd := exec.Command(command[0], command[1], filepath.Join(oldPath, filename), filepath.Join(newPath, filename))
				err := cpCmd.Run()
				if err != nil {
					log.Println(err)
				}

			}
		}
	}
}
